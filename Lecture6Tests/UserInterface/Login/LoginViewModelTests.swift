//
//  LoginViewModelTests.swift
//  lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import XCTest
@testable import Lecture6

class LoginViewModelTests: XCTestCase {

    class LoginServiceMock: LoginService {
        var performloginInvoked = false

        override func login(email: String, password: String, completion: @escaping  (Bool, Error?) -> () ) {
            performloginInvoked = true
            completion(true, nil)
        }
    }

    class PersistServiceStub: PersistService {
        private var _email: String? = "testEmail"
        override var email: String? {
            get {
                return _email
            }

            set {
                _email = newValue
            }
        }
    }

    var viewModel: LoginViewModel!
    var loginService: LoginServiceMock!
    var persistService: PersistService!
    
    override func setUp() {
        super.setUp()
        loginService = LoginServiceMock()
        persistService = PersistServiceStub()
        viewModel = LoginViewModel(loginService: loginService, persistService: persistService)
    }
    
    override func tearDown() {
        viewModel = nil
        loginService = nil
        super.tearDown()
    }

    //Outgoing query
    func testEmailFieldIsSet() {
        XCTAssertEqual("testEmail", viewModel.email)
    }

    // Incoming command
    func testErrorMessageClearsOnSettingPassword() {
        viewModel.errorMessage = "Invalid password"
        viewModel.password = "password"
        XCTAssertEqual(viewModel.errorMessage, "")
    }

    // Outgoing command
    func testLoginServiceInvokedinPerformLogin() {
        let performLoginExpectation = expectation(description: "wait for perform login")

        viewModel.performLogin(completion: { success in
            performLoginExpectation.fulfill()
        })

        waitForExpectations(timeout: 1) { error in
             XCTAssertTrue(self.loginService.performloginInvoked)
        }
    }

    // Incoming command
    func testLoginButtonDisabledIfEmailInvalid() {
        viewModel.email = ""
        viewModel.password = "password"
        XCTAssertEqual(false, viewModel.isLoginButtonEnabled)
    }

    // Incoming command
    func testLoginEnabledIfEmailAndPasswordValidAndLoginNOtInProgressAlready() {
        viewModel.email = "test@test.test"
        viewModel.password = "password"

        XCTAssertTrue(viewModel.isLoginButtonEnabled)
    }

    // Incoming command
    func testLoginButtonDisabledIfPasswordInvalid() {
        viewModel.email = "test@test.com"
        viewModel.password = ""

        XCTAssertEqual(false, viewModel.isLoginButtonEnabled)
    }

    // Incoming command
    func testEmailFieldDisabledWhileLoginInProgress() {
        viewModel.isLoginRequestInProgress = true

        XCTAssertFalse(viewModel.isEmailFieldEnabled)
    }
}
