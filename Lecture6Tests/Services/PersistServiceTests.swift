//
//  PersistServiceTests.swift
//  lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import XCTest
import Foundation
@testable import Lecture6

class PersistServiceTests: XCTestCase {

    var persistService: PersistService!

    override func setUp(){
        super.setUp()
        persistService = PersistService()
    }

    override func tearDown() {
        persistService = nil
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        super.tearDown()
    }

    // Incoming query
    func testEmailGet() {
        UserDefaults.standard.set("test@test.test", forKey: "email")
        XCTAssertEqual(persistService.email,"test@test.test")
    }


    func testEmailSet() {
        persistService.email = "test@test.test"
        XCTAssertEqual(UserDefaults.standard.string(forKey: "email") , "test@test.test")
    }

}
