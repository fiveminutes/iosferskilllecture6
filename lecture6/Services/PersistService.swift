//
//  PersistService.swift
//  lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import Foundation

class PersistService {

    private let userDefaults = UserDefaults.standard

    var email: String? {
        get {
            return userDefaults.string(forKey: "email")
        }

        set {
            userDefaults.set(newValue, forKey:"email")
        }
    }

}
