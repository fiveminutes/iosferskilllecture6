//
//  LoginService.swift
//  Lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import Foundation


class LoginService {

    func login(email: String, password: String, completion: @escaping  (Bool, Error?) -> () ) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) { 
            completion(password == "password", nil)
        }
    }

}
