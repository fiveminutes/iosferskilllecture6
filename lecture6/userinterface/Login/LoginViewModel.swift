//
//  LoginViewModel.swift
//  Lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import Foundation


class LoginViewModel {

    // Inputs
    var email = "" {
        didSet {
            errorMessage = ""
        }
    }
    var password = "" {
        didSet {
            errorMessage = ""
        }
    }

    // Outputs
    var isLoginButtonEnabled: Bool {
        return email.characters.count > 3 && password.characters.count > 5 && !isLoginRequestInProgress
    }

    var showSpinner: Bool {
        return isLoginRequestInProgress
    }

    var errorMessage = ""
    var showErrorMessage: Bool {
        return !errorMessage.isEmpty
    }

    var isEmailFieldEnabled: Bool {
        return !isLoginRequestInProgress
    }

    var isPasswordFieldEnabled: Bool {
        return !isLoginRequestInProgress
    }

    internal var isLoginRequestInProgress = false
    private let loginService: LoginService
    private let persistService: PersistService

    init(loginService: LoginService, persistService: PersistService) {
        self.loginService = loginService
        self.persistService = persistService
        email = persistService.email ?? ""
    }

    func performLogin(completion: @escaping (Bool) -> Void) {
        isLoginRequestInProgress = true
        loginService.login(email: email, password: password, completion: { (success, error) in
            self.isLoginRequestInProgress = false
            if !success {
                self.errorMessage = "Invalid password"
            } else {
                self.persistService.email = self.email
            }
            completion(success)
        })
    }
}
