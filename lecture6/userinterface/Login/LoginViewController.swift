//
//  LoginviewController.swift
//  iOS-vjestina-arhitektura
//
//  Created by Mario Radonic on 3/14/17.
//  Copyright © 2017 Mario. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!

    private var loginBarButtonItem: UIBarButtonItem!

    private var showErrorLabel = false
    private var showSpinner = false

    var viewModel: LoginViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Welcome"
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.rightBarButtonItem = loginBarButtonItem
        loginBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(loginBarButtonTapped))

        emailTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)

        emailLabel.font = UIFont.systemFont(ofSize: 12)
        passwordLabel.font = UIFont.systemFont(ofSize: 12)
        emailLabel.textColor = UIColor(red: 12/255, green: 28/255, blue: 12/255, alpha: 1)
        passwordLabel.textColor = UIColor(red: 12/255, green: 28/255, blue: 12/255, alpha: 1)

        navigationItem.rightBarButtonItem = loginBarButtonItem
        activityIndicatorView.startAnimating()

        emailTextField.text = viewModel.email

        updateUI()
    }

    @objc func textFieldDidChange(sender: UITextField) {
        let text = sender.text ?? ""
        switch sender {
        case emailTextField:
            viewModel.email = text
        case passwordTextField:
            viewModel.password = text
        default:
            break
        }
        updateUI()
    }

    @objc func loginBarButtonTapped() {
        sendLoginRequest()
        updateUI()
    }

    func updateUI() {
        loginBarButtonItem.isEnabled = viewModel.isLoginButtonEnabled

        errorLabel.text = viewModel.errorMessage
        errorLabel.isHidden = !viewModel.showErrorMessage

        passwordTextField.backgroundColor = viewModel.showErrorMessage ? UIColor.red : UIColor.clear

        activityIndicatorView.isHidden = !viewModel.showSpinner
        passwordTextField.isEnabled = viewModel.isPasswordFieldEnabled
        emailTextField.isEnabled = viewModel.isEmailFieldEnabled
    }

    func sendLoginRequest() {
        viewModel.performLogin { (success) in
            self.updateUI()
            if success {
                self.navigationController?.pushViewController(HomeViewController(), animated: true)
            }
        }
    }
}
