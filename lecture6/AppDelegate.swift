//
//  AppDelegate.swift
//  Lecture6
//
//  Created by Leonard Beus on 14/03/2017.
//  Copyright © 2017 Five. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        let loginViewModel = LoginViewModel(loginService: LoginService(), persistService: PersistService())
        let loginViewController = LoginViewController()
        loginViewController.viewModel = loginViewModel

        let nvc = UINavigationController(rootViewController: loginViewController)
        window?.rootViewController = nvc
        window?.makeKeyAndVisible()

        return true
    }
}

